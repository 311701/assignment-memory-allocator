//
// Created by robq on 12/31/21.
//

#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H

void test_init_heap();

void test1_simple_mem_allocate();

void test2_one_block_free();

void test3_two_blocks_free();

void test4_new_region_extends_old();

void test5_new_region_another_place();

#endif //ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H
