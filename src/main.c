//
// Created by robq on 12/28/21.
//

#include "tests.h"
#include "mem_debug.h"

int main() {
    test_init_heap();
    test1_simple_mem_allocate();                        //Обычное успешное выделение памяти.
    test2_one_block_free();                             //Освобождение одного блока из нескольких выделенных.
    test3_two_blocks_free();                            //Освобождение двух блоков из нескольких выделенных
    test4_new_region_extends_old();                      //Память закончилась, новый регион памяти расширяет старый.
    test5_new_region_another_place();                    //Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте
    debug("\n......... Все тесты успешно выполнены ..........\n");
}

