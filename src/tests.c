//
// Created by robq on 12/31/21.
//

#define _DEFAULT_SOURCE
#define INITIAL_HEAP_SIZE 10240
#define COLOUR_BLUE "\033[0;36m"
#define COLOUR_GREEN "\033[0;32m"
#define COLOUR_RED "\033[1;31m"
#define COLOUR_DEFAULT "\033[0m"
#include "tests.h"

#include <stdio.h>
#include <inttypes.h>
#include <unistd.h>

#include "mem.h"
#include "mem_debug.h"
#include "mem_internals.h"
#include "util.h"

static void* heap;

static struct block_header* start_block;

void debug_header(const char *msg){
    debug(COLOUR_BLUE);
    debug(msg);
    debug(COLOUR_DEFAULT);
}

void debug_succes(const char *msg){
    debug(COLOUR_GREEN);
    debug(msg);
    debug(COLOUR_DEFAULT);
}

void error(const char *msg){
    debug(COLOUR_RED);
    err(msg);
    debug(COLOUR_DEFAULT);
}



static inline struct block_header* get_block_by_allocated_data(void* data) {
    return (struct block_header *) ((uint8_t *) data - offsetof(struct block_header, contents));
}



void test_init_heap(){
    heap = heap_init(INITIAL_HEAP_SIZE);
    if (heap == NULL) {
        error ("err: ошибка при инициализации кучи\n");
    } else {
        debug_heap (stdout, heap);
    }
    start_block = (struct block_header*) heap;
}

void test1_simple_mem_allocate() {
   
    debug_header("<<< Тест 1 : обычное выделение памяти >>>\n");
    void* test1_mem = _malloc(1000);
    if (test1_mem == NULL) 
        error ("err: ошибка при выделении памяти\n");

    debug_heap (stdout, heap);

    if (start_block->capacity.bytes != 1000) 
        error("err: Размер не соответствует выделенному\n");
    if ( start_block->is_free) 
        error ("errБлок не помечен, как занятый\n");
    
    debug_succes("Тест 1 пройден\n\n");
}

void test2_one_block_free() {
    debug_header("<<< Тест 2: Освобождение одного блока из нескольких выделенных >>>\n");
    void* test2_mem1 = _malloc(1000);
    void* test2_mem2 = _malloc(1000);
    debug("До освобождения\n");
    debug_heap(stdout, heap);
    _free(test2_mem1);
    debug("После\n");
    debug_heap(stdout, heap);
    struct block_header *data_block1 = get_block_by_allocated_data(test2_mem1), *data_block2 = get_block_by_allocated_data(test2_mem2);
    if (!data_block1->is_free) {
        error("err: Свободный блок оказался занятым\n");
    }
    if (data_block2->is_free) {
        error("err: Занятый блок оказался свободным\n");
    }

    debug_succes("Тест 2 пройден\n\n");
    _free(test2_mem1);
    _free(test2_mem2);

}

void test3_two_blocks_free() {
     
    debug_header("<<< Тест 3: Освобождение двух блоков из нескольких выделенных >>>\n");

    const size_t s1 = 512, s2 = 1024, s3 = 2048;

    void *test3_mem1 = _malloc(s1), *test3_mem2 = _malloc(s2), *test3_mem3 = _malloc(s3);
    
    if (test3_mem1 == NULL || test3_mem2 == NULL || test3_mem3 == NULL) {
        error("err: ошибка при выделении памяти\n");
    }
    debug("До освобождения\n");
    debug_heap(stdout, heap);

    
    //Освободим два блока подряд, а потом проверим, объединились ли они
    _free(test3_mem2);
    _free(test3_mem1);

    debug("После\n");
    debug_heap(stdout, heap);
    struct block_header *data_block1 = get_block_by_allocated_data(test3_mem1), *data_block3 = get_block_by_allocated_data(test3_mem3);
    if (!data_block1->is_free) {
        error("err: Свободный блок оказался занятым\n");
    }
    if (data_block3->is_free) {
        error("err: Занятый блок оказался свободным\n");
    }
    if (data_block1->capacity.bytes != s1 + s2 + offsetof(struct block_header, contents)) {
        error("err: два свободных блока не соединялись\n");
    }
    debug_succes("Тест 3 пройден\n\n");

    _free(test3_mem1);
    _free(test3_mem2);
    _free(test3_mem3);




}

static void free_heap() {
    struct block_header* curr = start_block;
    while (curr != NULL) {
        if (!curr->is_free) {
            _free((void*) (((uint8_t*) curr) + offsetof(struct block_header, contents)));
            curr = curr->next;
        }
    }
}


void test4_new_region_extends_old() {
    free_heap();
    debug_header("<<< Тест 4: Память закончилась, новый регион памяти расширяет старый >>>\n");

    void *test4_mem1 = _malloc(INITIAL_HEAP_SIZE), *test4_mem2 = _malloc(INITIAL_HEAP_SIZE + 512);
    if (test4_mem1 == NULL || test4_mem2 == NULL ) {
        error("err: ошибка при выделении памяти\n");
    }

    debug_heap(stdout, heap);

    
    _free(test4_mem2);

    

    struct block_header *data_block1 = get_block_by_allocated_data(test4_mem1), *data_block2 = get_block_by_allocated_data(test4_mem2);

    if ((uint8_t *)data_block1->contents + data_block1->capacity.bytes != (uint8_t*) data_block2){
        error("err: новый регион не был создан после последнего\n");
    }
    debug_succes("Тест 4 пройден\n\n");

    _free(test4_mem1);
    _free(test4_mem2);
    

}

struct block_header* last_block() {
    for (struct block_header* last = start_block; ; last = last->next) {
        if (last->next == NULL) {
            return last;
        }
    }
}




void test5_new_region_another_place() {
    
    debug_header ("<<< Тест 5: Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте >>>\n");

    struct block_header* last = last_block();

    void* test5_taken_mem_addr = last + size_from_capacity(last->capacity).bytes;

    void* test5_taken_mem = mmap( (uint8_t*) (getpagesize() * ((size_t) test5_taken_mem_addr / getpagesize() +
                                                            (((size_t) test5_taken_mem_addr % getpagesize()) > 0))), 1000,
            PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE,0, 0);


    
    debug(" addr: %p\n", test5_taken_mem);
    void* test5_mem=_malloc(40000);

    while(test5_mem<test5_taken_mem){

        test5_mem=_malloc(40000);
        
        struct block_header *data_block = get_block_by_allocated_data(test5_mem);

        if (test5_mem == start_block->next || test5_mem == test5_taken_mem ||
                data_block->capacity.bytes != 40000 ||
                data_block->is_free) {
            error("err: new region another place test failed\n");
        }
    }

    

    debug_heap(stdout, heap);

    debug_succes ("Тест 5 пройден\n");
}